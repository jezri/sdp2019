<?php
//include_once('src/classes/renderable.php');
require_once('src/classes/renderable.php');
use PHPUnit\Framework\TestCase;
    class NewTest extends TestCase {

    //Test defualt values for report_witslog_renderable //
    //@Test
    public function test_defualt_show_form(){ 
        $myRend = new report_witslog_renderable();
      }
     
     public function test_defualt_url(){ 
        $myRend = new report_witslog_renderable();
      }

     
     public function test_defualt_show_logs(){ 
        $myRend = new report_witslog_renderable();
      }
     
     public function test_defualt_date(){ 
        $myRend = new report_witslog_renderable();
      }

     public function test_defualt_starttime_test(){ 
        $myRend = new report_witslog_renderable();
      }
     
     public function test_defualt_endtime(){ 
        $myRend = new report_witslog_renderable();
      }

     public function test_defualt_courseid(){ 
        $myRend = new report_witslog_renderable();
      }

     public function test_defualt_showaccounts(){ 
        $myRend = new report_witslog_renderable();
      }

     public function test_defualt_showatips(){ 
        $myRend = new report_witslog_renderable();
      }

     // Now test the constructor with non defualt values
     
     public function test_init_show_form(){ 
        $myRend = new report_witslog_renderable(false, true, 'myUrl', 1, 1, 1, 1, true, true) ;
        $this->assertEquals($myRend->show_form, false);
      }
       
     public function test_init_url(){ 
        $myRend = new report_witslog_renderable(false, true, 'myUrl', 1, 1, 1, 1, true, true) ;
        $this->assertEquals($myRend->url, 'myUrl');
      }
     
     public function test_init_show_logs(){ 
        $myRend = new report_witslog_renderable(false, true, 'myUrl', 1, 1, 1, 1, true, true) ;
        $this->assertEquals($myRend->show_logs, true);
      }
     
     public function test_init_date(){ 
        $myRend = new report_witslog_renderable(false, true, 'myUrl', 1, 1, 1, 1, true, true) ;
        $this->assertEquals($myRend->date, 1);
      }

     public function time_init_starttime(){ 
        $myRend = new report_witslog_renderable(false, true, 'myUrl', 1, 1, 1, 1, true, true) ;
        $this->assertEquals($myRend->starttime, 1);
      }
     
     public function test_init_endtime(){ 
        $myRend = new report_witslog_renderable(false, true, 'myUrl', 1, 1, 1, 1, true, true) ;
        $this->assertEquals($myRend->endtime, 1);
      }

     public function test_init_courseid(){ 
        $myRend = new report_witslog_renderable(false, true, 'myUrl', 1, 1, 1, 1, true, true) ;
        $this->assertEquals($myRend->courseid, 1);
      }

     public function test_init_showaccounts(){ 
        $myRend = new report_witslog_renderable(false, true, 'myUrl', 1, 1, 1, 1, true, true) ;
        $myRend->setup_table();
        $this->assertEquals($myRend->showaccounts, true);
      }

     // Test for get user list
     public function get_user_list_test(){
       $myRend = new report_witslog_renderable();
       $myRend->setup_table();
       $myList = $myRend->get_user_list();
       $this->assertEquals($myList, []);
     }

     public function  get_course_list(){
       $myRend = new report_witslog_renderable();
       $myRend->setup_table();
       echo $myRend->get_course_list();
       $this->assertEquals($myRend->get_course_list(),[]);
     }

     public function get_data_options(){
       $myRend = new report_witslog_renderable();
       echo $myRend->get_date_options();
       $myRend->setup_table();
       $this->assertEquals($myRend->get_date_options(),[]);
     }

     public function test_construct_wits_log(){
    $myReport = new report_witslog_table_witslog("my_unique_id");
    $this->assertNotNull($myReport);
     }


/*     public function test_renderer(){
      $myReport = new report_witslog_table_witslog("my_unique_id");
      $myRender=new report_witslog_renderer($myPage, "arg2");
      $this->assertNotNull($myRender);
       
}*/

 }
     

?>
