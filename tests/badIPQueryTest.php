<?php
    use PHPUnit\Framework\TestCase;
    
    class BadQueryTests extends TestCase {

        //test that start time withihn first period returns expected records
     public function testStartTimeFirst() { //RENAME WHEN ACTUAL FUNCTION IS DEFINED
            date_default_timezone_set('Africa/Johannesburg');
            $start_time = date("Y-m-d H:i:s", 1554357600);  //2019-04-04 08:00:00
            $end_time = date("Y-m-d H:i:s", 1554364800);    //2019-04-04 10:00:00
            
            $result = get_bad_ip_addresses($start_time, $end_time);

            foreach($result as $row) {
                $this->assertGreaterThanOrEqual($start_time, $row['timecreated']);
            }
        }

        //test that end time withihn first period returns expected records
        public function testEndTimeFirst() { //RENAME WHEN ACTUAL FUNCTION IS DEFINED
            date_default_timezone_set('Africa/Johannesburg');
            $start_time = date("Y-m-d H:i:s", 1554357600);  //2019-04-04 08:00:00
            $end_time = date("Y-m-d H:i:s", 1554364800);    //2019-04-04 10:00:00
            
            $result = get_bad_ip_addresses($start_time, $end_time);

            foreach($result as $row) {
                $this->assertLessThanOrEqual($end_time, $row['timecreated']);
            }
        }

        //test that interval which partially covers existing records returns expected records
        public function testTimeIntersection() {
            date_default_timezone_set('Africa/Johannesburg');
            $start_time = date("Y-m-d H:i:s", 1554447600);  //2019-04-05 09:00:00
            $end_time = date("Y-m-d H:i:s", 1554454800);    //2019-04-05 11:00:00
            
            $result = get_bad_ip_addresses($start_time, $end_time);

            foreach($result as $row) {
                $this->assertLessThanOrEqual($end_time, $row['timecreated']);
            }
        }

        //test that interval outside of any recorded period returns nothing
        public function testTimeOutOfBounds() {
            date_default_timezone_set('Africa/Johannesburg');
            $start_time = date("Y-m-d H:i:s", 1554368400);  //2019-04-04 11:00:00
            $end_time = date("Y-m-d H:i:s", 1554375600);    //2019-04-04 13:00:00

            $result = get_bad_ip_addresses($start_time, $end_time);

            $this->assertEquals(0, $result->num_rows);
        }
        //test correct course returned for an existing course
        public function testCourseExists() {
            //whole first period
            date_default_timezone_set('Africa/Johannesburg');
            $start_time = date("Y-m-d H:i:s", 1554357600);  //2019-04-04 08:00:00
            $end_time = date("Y-m-d H:i:s", 1554364800);    //2019-04-04 10:00:00
            
            $course = 1;
            $coursename = "Course A";

            $result = get_bad_ip_addresses($start_time, $end_time, $course);

            foreach($result as $row) {
                $this->assertEquals($coursename, $row['coursename']);
            }
        }

        //test query with nonexistant course returns an empty result
        public function testCourseNonExistant() {
            //whole first period
            date_default_timezone_set('Africa/Johannesburg');
            $start_time = date("Y-m-d H:i:s", 1554357600);  //2019-04-04 08:00:00
            $end_time = date("Y-m-d H:i:s", 1554364800);    //2019-04-04 10:00:00

            $course = 6;

            $result = get_bad_ip_addresses($start_time, $end_time, $course);

            $this->assertEquals(0, $result->num_rows);
        }
        //test that all suspicious IP addresses returned without course specified
        public function testBadIPNoCourse() {
            //whole first period
            date_default_timezone_set('Africa/Johannesburg');
            $start_time = date("Y-m-d H:i:s", 1554357600);  //2019-04-04 08:00:00
            $end_time = date("Y-m-d H:i:s", 1554364800);    //2019-04-04 10:00:00

            $result = get_bad_ip_addresses($start_time, $end_time);

            $ip_expected = array('197.91.172.109', '41.247.187.14');
            $ip_actual = array();

            echo "\n\n\n".$result->num_rows."\n\n\n";

            foreach($result as $row) {
                array_push($ip_actual, $row['ip']);
            }

            $this->assertEquals(sort($ip_expected), sort($ip_actual));
        }

        //test that all suspicious IP addresses returned with course specified
       public function testBadIPWithCourse() {
            //whole first period
            date_default_timezone_set('Africa/Johannesburg');
            $start_time = date("Y-m-d H:i:s", 1554357600);  //2019-04-04 08:00:00
            $end_time = date("Y-m-d H:i:s", 1554364800);    //2019-04-04 10:00:00

            $course = 1;

            $result = get_bad_ip_addresses($start_time, $end_time, $course);

            $ip_expected = array('197.91.172.109', '41.247.187.14');
            $ip_actual = array();

            echo "\n\n\n".$result->num_rows."\n\n\n";

            foreach($result as $row) {
                array_push($ip_actual, $row['ip']);
            }

            $this->assertEquals(sort($ip_expected), sort($ip_actual));
        }
        
    }
?>
