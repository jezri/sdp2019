<?php
    use PHPUnit\Framework\TestCase;
    
    class BadIPTests extends TestCase {
   //      public function setUp() {
    //        date_default_timezone_set('Africa/Johannesburg');
     //    }

        //test that start time withihn first period returns expected records
     /*  public function testStartTimeFirst() { //RENAME WHEN ACTUAL FUNCTION IS DEFINED
            $start_time = date("Y-m-d H:i:s", 1554357600);  //2019-04-04 08:00:00
            $end_time = date("Y-m-d H:i:s", 1554364800);    //2019-04-04 10:00:00
            
            $result = get_bad_accounts($start_time, $end_time);

            foreach($result as $row) {
                $this->assertGreaterThanOrEqual($start_time, date("Y-m-d H:i:s", $row['timecreated']));
            }
     } */
        //test that end time withihn first period returns expected records
        public function testEndTimeFirst() { //RENAME WHEN ACTUAL FUNCTION IS DEFINED
            $start_time = date("Y-m-d H:i:s", 1554357600);  //2019-04-04 08:00:00
            $end_time = date("Y-m-d H:i:s", 1554364800);    //2019-04-04 10:00:00
            
            $result = get_bad_accounts($start_time, $end_time);

            foreach($result as $row) {
                $this->assertLessThanOrEqual($end_time, date("Y-m-d H:i:s", $row['timecreated']));
            }
        }

        //test that interval which partially covers existing records returns expected records
        public function testTimeIntersection() {
            $start_time = date("Y-m-d H:i:s", 1554447600);  //2019-04-05 09:00:00
            $end_time = date("Y-m-d H:i:s", 1554454800);    //2019-04-05 11:00:00
            
            $result = get_bad_accounts($start_time, $end_time);

            foreach($result as $row) {
                $this->assertLessThanOrEqual($end_time, date("Y-m-d H:i:s", $row['timecreated']));
            }
        }

        //test that interval outside of any recorded period returns nothing
        public function testTimeOutOfBounds() {
            $start_time = date("Y-m-d H:i:s", 1554368400);  //2019-04-04 11:00:00
            $end_time = date("Y-m-d H:i:s", 1554375600);    //2019-04-04 13:00:00

            $result = get_bad_accounts($start_time, $end_time);

            $this->assertEquals(0, $result->num_rows);
        }

        //test correct course returned for an existing course
        public function testCourseExists() {
            //whole second period
            $start_time = date("Y-m-d H:i:s", 1554451200);  //2019-04-05 10:00:00
            $end_time = date("Y-m-d H:i:s", 1554472800);    //2019-04-05 16:00:00

            $course = 1;

            $result = array();
            foreach($result as $row) {
                $this->assert_equal($course, $row['course']);
            }
        }

        //test query with nonexistant course returns an empty result
       public function testCourseNonExistant() {
            //whole second period
            $start_time = date("Y-m-d H:i:s", 1554451200);  //2019-04-05 10:00:00
            $end_time = date("Y-m-d H:i:s", 1554472800);    //2019-04-05 16:00:00

            $course = 6;

            $result = get_bad_accounts($start_time, $end_time, $course);

            $this->assertEquals(0, $result->num_rows);
        }

        //test that all suspicious accounts returned without course specified
    /*   public function testBadAccountNoCourse() {
            //whole second period
            $start_time = date("Y-m-d H:i:s", 1554451200);  //2019-04-05 10:00:00
            $end_time = date("Y-m-d H:i:s", 1554472800);    //2019-04-05 16:00:00

            $result = get_bad_accounts($start_time, $end_time);

            $account_expected = array(3, 4, 6);
            $account_actual = array();

            foreach($result as $row) {
                array_push($ip_actual, $row['userid']);
            }

            $this->assertEquals($account_expected, $account_actual);
        }

        //test that all suspicious accounts returned with course specified
        public function testBadAccountCourse2() {
            //first sub-period
            $start_time = date("Y-m-d H:i:s", 1554451200);  //2019-04-05 10:00:00
            $end_time = date("Y-m-d H:i:s", 1554458400);    //2019-04-05 12:00:00

            $course = 2;

            $result = get_bad_accounts($start_time, $end_time, $course);

            $account_expected = array(4);
            $account_actual = array();

            foreach($result as $row) {
                array_push($ip_actual, $row['userid']);
            }

            $this->assertEquals(sort($account_expected), sort($account_actual));
        }


        //test that all suspicious accounts returned with course specified
        public function testBadAccountCourse3() {
            //first sub-period
            $start_time = date("Y-m-d H:i:s", 1554451200);  //2019-04-05 10:00:00
            $end_time = date("Y-m-d H:i:s", 1554458400);    //2019-04-05 12:00:
            
            $course = 3;

            $result = get_bad_accounts($start_time, $end_time, $course);

            $account_expected = array(6);
            $account_actual = array();

            foreach($result as $row) {
                array_push($ip_actual, $row['userid']);
            }

            $this->assertEquals(sort($account_expected), sort($account_actual));
        }*/
    }
    
?>
