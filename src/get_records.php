<?php
    function get_bad_ip_addresses($start_time, $end_time, $course=NULL) {
        date_default_timezone_set('Africa/Johannesburg');

        require "src/db_connect.php";

        $query = "";

        if ($course == NULL) {
            $query = sprintf("SELECT lg.ip, CONCAT(usr.firstname, ' ', usr.lastname) AS username, FROM_UNIXTIME(lg.timecreated) as timecreated FROM mdl_logstore_standard_log lg JOIN mdl_user usr ON lg.userid = usr.id WHERE lg.timecreated BETWEEN UNIX_TIMESTAMP('%s') AND UNIX_TIMESTAMP('%s') GROUP BY lg.ip, usr.id, lg.timecreated", $start_time, $end_time);
        } else {
            $query = sprintf("SELECT lg.ip, CONCAT(usr.firstname, ' ', usr.lastname) AS username, crs.fullname AS coursename, FROM_UNIXTIME(lg.timecreated) as timecreated FROM mdl_logstore_standard_log lg INNER JOIN mdl_user usr ON lg.userid = usr.id INNER JOIN mdl_course crs ON lg.courseid = crs.id WHERE ((lg.timecreated BETWEEN UNIX_TIMESTAMP('%s') AND UNIX_TIMESTAMP('%s'))) AND lg.courseid = %d GROUP BY lg.ip, usr.id, lg.timecreated, crs.id", $start_time, $end_time, $course);
        }

        echo "\n\n\n".$query."\n\n\n";

        $result = $conn->query($query);

        echo "\n\n\nNUM_ROWS: ".$result->num_rows."\n\n\n";

        return $result;
    }

    function get_bad_accounts($start_time, $end_time, $course=NULL) {
        date_default_timezone_set('Africa/Johannesburg');

        require "src/db_connect.php";

        $query = "";

        if ($course == NULL) {
            $query = sprintf("SELECT lg.ip, CONCAT(usr.firstname, ' ', usr.lastname) AS username, FROM_UNIXTIME(lg.timecreated) as timecreated FROM mdl_logstore_standard_log lg JOIN mdl_user usr ON lg.userid = usr.id WHERE lg.timecreated BETWEEN UNIX_TIMESTAMP('%s') AND UNIX_TIMESTAMP('%s') GROUP BY lg.ip, usr.id, lg.timecreated", $start_time, $end_time);
        } else {
            $query = sprintf("SELECT lg.ip, CONCAT(usr.firstname, ' ', usr.lastname) AS username, crs.fullname AS coursename, FROM_UNIXTIME(lg.timecreated) as timecreated FROM mdl_logstore_standard_log lg INNER JOIN mdl_user usr ON lg.userid = usr.id INNER JOIN mdl_course crs ON lg.courseid = crs.id WHERE ((lg.timecreated BETWEEN UNIX_TIMESTAMP('%s') AND UNIX_TIMESTAMP('%s'))) AND lg.courseid = %d GROUP BY lg.ip, usr.id, lg.timecreated, crs.id", $start_time, $end_time, $course);
        }

        echo "\n\n\n".$query."\n\n\n";

        $result = $conn->query($query);

        echo "\n\n\nNUM_ROWS: ".$result->num_rows."\n\n\n";

        return $result;

    }
?>
