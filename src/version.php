<?php
	defined('MOODLE_INTERNAL') || die;
	
	$plugin->version   = 2019041641;         // The current plugin version (Date: YYYYMMDDXX)
	$plugin->requires  = 2018112800;         // Requires this Moodle version
	$plugin->component = 'report_witslog'; // Full name of the plugin (used for diagnostics)
?>
