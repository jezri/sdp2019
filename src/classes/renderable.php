<?php

//defined('MOODLE_INTERNAL') || die;

class report_witslog_renderable implements renderable {
    //boolean | show/don't show selector form
    public $show_form;

    //boolean | show/don't show log table
    public $show_logs;

    //string | page url
    public $url;

    //int (unix timestamp) | selected date
    public $date;

    //int (unix timestamp) | selected start time
    public $starttime;

    //int (unix timestamp) | selected start time
    public $endtime;

    //int | courseid selected
    public $courseid;

    //boolean | show bad accounts
    public $showaccounts;

    //boolean | show bad ip addresses
    public $showips;

    //report_witslog_table_witslog | log table
    public $table;

    public function __construct($show_form = true, $show_logs = false, $url = '', $date = 0, $starttime = 0, $endtime = 0, $courseid = 0, $showaccounts = false, $showips = false) {

            //if url is unset, use the page url
            if (empty($url)) {
                $url = new moodle_url($PAGE->url);
            } else {
                $url = new moodle_url($url);
            }

            $this->url          = $url;
            $this->show_form    = $show_form;
            $this->show_logs    = $show_logs;
            $this->date         = $date;
            $this->starttime    = $starttime;
            $this->endtime      = $endtime;
            $this->courseid     = $courseid;
            $this->showaccounts = $showaccounts;
            $this->showips      = $showips;
    }

    public function setup_table() {
        //stdClass used as associative array holding filter of data to be rendered in the table
        $filter = new \stdClass();

        $filter->courseid     = $this->courseid;
        $filter->date         = $this->date;
        $filter->starttime    = $this->starttime;
        $filter->endtime      = $this->endtime;
        $filter->showaccounts = $this->showaccounts;
        $filter->showips      = $this->showips;

        $this->table = new report_witslog_table_witslog('report_witslog', $filter);
        $this->table->define_baseurl($this->url);
    }

    //Probably won't need this
    public function get_user_list() {
        global $SITE;

        $course_id = $SITE->id;
        $context = context_course::instance($course_id);
        $user_records = get_enrolled_users($context);

        $users = array();
        foreach($user_records as $user) {
            $users[$user->id] = fullname($user);
        }

        return $users;
    }

    //Taken from report/log/classes/renderable.php
    //helper function used to get date options to be displayed in select menu
    public function get_date_options() {
        global $SITE;

        $strftimedate = get_string("strftimedate");
        $strftimedaydate = get_string("strftimedaydate");

        // Get all the possible dates.
        // Note that we are keeping track of real (GMT) time and user time.
        // User time is only used in displays - all calcs and passing is GMT.
        $timenow = time(); // GMT.

        // What day is it now for the user, and when is midnight that day (in GMT).
        $timemidnight = usergetmidnight($timenow);

        // Put today up the top of the list.
        $dates = array("$timemidnight" => get_string("today").", ".userdate($timenow, $strftimedate) );

        // If course is empty, get it from frontpage.
        $course = $SITE;
        // if (!empty($this->course)) {
        //     $course = $this->course;
        // }
        if (!$course->startdate or ($course->startdate > $timenow)) {
            $course->startdate = $course->timecreated;
        }

        $numdates = 1;
        while ($timemidnight > $course->startdate and $numdates < 365) {
            $timemidnight = $timemidnight - 86400;
            $timenow = $timenow - 86400;
            $dates["$timemidnight"] = userdate($timenow, $strftimedaydate);
            $numdates++;
        }
        return $dates;
    }

    //helper function used to get time options to be displayed in select menu
    public function get_time_options($base_date, $is_starttime = false) {
        $times = array();

        $date_format = get_string('strftimetime24', 'langconfig');

        $next_day_timestamp = usergetmidnight($base_date + 86400);
        
        $timestamp = $base_date;
        if ($is_starttime) {
            $timestamp += 1800;
        }

        while ($timestamp < $next_day_timestamp) {
            $times[$timestamp] = userdate($timestamp, $date_format);
            $timestamp += 1800;
        }

        if ($is_starttime) {
            $times[$next_day_timestamp - 60] = userdate($next_day_timestamp - 60, $date_format);
        }

        return $times;
    }

    //helper function use to get courses to be displayed in select menu
    public function get_course_list() {
        global $DB;

        $courses = array();

        //Taken from report/log/classes/renderable.php
        $courserecords = $DB->get_records("course", null, "fullname", "id,shortname,fullname,category");

        foreach ($courserecords as $course) {
            if ($course->id == SITEID) {
                $courses[$course->id] = format_string($course->fullname) . ' (' . get_string('site') . ')';
            } else {
                $courses[$course->id] = format_string(get_course_display_name_for_list($course));
            }
        }
        core_collator::asort($courses);

        return $courses;
    }
}

