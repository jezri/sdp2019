<html>
<body>
<?php
require_once("config.php");


function my_print_table($result){
	echo "<table border='1'>";
	echo "<tr>";
	foreach($result as $row){
		foreach($row as $col => $val){
			echo "<td>" . $col . "</td>";
		}
		break;
	}
	echo "</tr>";
	foreach($result as $id => $row){
		echo "<tr>";
		foreach($row as $col => $val){
			echo "<td>" . $val . "</td>";
		}
		echo "<tr/>";
	}
	echo "</table>";
}

$day=$_GET["day"];
$start=$_GET["start_time"];
$end=$_GET["end_time"];
$courseid=$_GET["courseid"];
$ipfil=$_GET["ip"];
$namefil=$_GET["name"];
$res = $DB->get_records_sql("SELECT id, shortname, fullname FROM moodle.mdl_course;");
$select = "<select name='courseid'>";
foreach($res as $row){
	$id=$row->id;
	$name=$row->shortname . " (" . $row->fullname . ")";
	$sel="";
	if($courseid == $id){
		$sel=" selected='selected' ";
	} 
	$select .= "<option value='$id' $sel>$name</option>";
}
$select .= "</select>";

echo<<<EOF
	<form method='get' action='#'>
	  Date:
	  <input type="date" name="day" value="$day">
	  Start:
	  <input type="time" name="start_time" value="$start">
	  Finish:
	  <input type="time" name="end_time" value="$end">
	  $select
	  <input type="text" name="ip" value="$ipfil">
	  <input type="text" name="name" value="$namefil">
	  <input type="submit" name="mode" value="log">
	  <input type="submit" name="mode" value="ip">
	</form>
EOF;

if(!isset($_GET["day"]) || !isset($_GET["start_time"]) || !isset($_GET["end_time"]) || !isset($_GET["courseid"])){
	die();
}
$mode="log";
if(isset($_GET["mode"])){
	$mode = $_GET["mode"];
}

$name_filter="%";
if(isset($_GET["name"])){
	$name_filter = "%" . $_GET["name"] . "%";
}


$ip_filter="%";
if(isset($_GET["ip"])){
	$ip_filter = "%" .  $_GET["ip"] . "%";
	$ip_filter = $_GET["ip"] ;
}


if($mode == "log"){
$result = $DB->get_records_sql("
SELECT l.id, FROM_UNIXTIME(`time`) AS 'date_formatted', l.ip, c.shortname, CONCAT(u.firstname, ' ', u.lastname) as name, l.module, l.action, 
	IF(l.module = 'assignment', a.name, IF(l.module = 'course', c.shortname, NULL)) as 'Info', l.url 
FROM moodle.mdl_log l
	JOIN moodle.mdl_course c ON l.course = c.id
        JOIN moodle.mdl_user u   ON u.id = l.userid
	LEFT OUTER JOIN moodle.mdl_assignment a ON l.info = a.id 
WHERE l.time >= UNIX_TIMESTAMP(STR_TO_DATE(?, '%Y-%m-%d %T')) 
  AND l.time <= UNIX_TIMESTAMP(STR_TO_DATE(?, '%Y-%m-%d %T'))
  AND l.course=? 
  AND l.ip LIKE ?
  AND CONCAT(u.firstname, ' ', u.lastname) LIKE ?;", array("$day $start", "$day $end","$courseid", "$ip_filter", "$name_filter"));

my_print_table($result);
}else{
$result = $DB->get_records_sql("
SELECT tt.ip, count(*)
FROM
(
SELECT @rank:=@rank+1 as 'rank', l.ip, CONCAT(u.firstname, ' ', u.lastname) as 'Name'
FROM moodle.mdl_log l
	JOIN moodle.mdl_course c ON l.course = c.id
        JOIN moodle.mdl_user u   ON u.id = l.userid
	LEFT OUTER JOIN moodle.mdl_assignment a ON l.info = a.id,
	(SELECT @rank:=0) as t
WHERE l.time >= UNIX_TIMESTAMP(STR_TO_DATE(?, '%Y-%m-%d %T')) 
  AND l.time <= UNIX_TIMESTAMP(STR_TO_DATE(?, '%Y-%m-%d %T'))
  AND l.course=?
GROUP BY l.ip, l.userid) tt
GROUP BY tt.ip 
HAVING count(*) > 1;", array("$day $start", "$day $end","$courseid"));

echo "<h1>Bad IPs</h1>IP addresses used by more than one account.<br/>";
//my_print_table($result);

$count = 0;
$bad_ips=array_keys($result);
foreach($bad_ips as $ip){
	$result = $DB->get_records_sql("
	SELECT @rank:=@rank+1 as 'rank', l.ip, CONCAT(u.firstname, ' ', u.lastname) as 'Name'
	FROM moodle.mdl_log l
		JOIN moodle.mdl_course c ON l.course = c.id
	        JOIN moodle.mdl_user u   ON u.id = l.userid
		LEFT OUTER JOIN moodle.mdl_assignment a ON l.info = a.id,
		(SELECT @rank:=0) as t
	WHERE l.time >= UNIX_TIMESTAMP(STR_TO_DATE(?, '%Y-%m-%d %T')) 
	  AND l.time <= UNIX_TIMESTAMP(STR_TO_DATE(?, '%Y-%m-%d %T'))
	  AND l.course=?
	  AND l.ip =? 
	GROUP BY l.ip, l.userid;", array("$day $start", "$day $end","$courseid", $ip));
	if($count == 0){
		echo "<table border='1'>";
		echo "<tr>";
		foreach($result as $row){
			foreach($row as $col => $val){
				echo "<td>" . $col . "</td>";
				$count = $count + 1;
			}
			break;
		}
		echo "</tr>";
	}

	foreach($result as $id => $row){
		echo "<tr>";
		foreach($row as $col => $val){
			echo "<td>" . $val . "</td>";
		}
		echo "<tr/>";
	}
	echo "<tr style='border-bottom: 5px solid; background-color: #EEEEEE;'><td colspan='$count'>&nbsp;</td></tr>";
}
echo "</table>";


$result = $DB->get_records_sql("
SELECT tt.userid, tt.Name, count(*)
FROM
(
SELECT @rank:=@rank+1 as 'rank', l.ip, l.userid, CONCAT(u.firstname, ' ', u.lastname) as 'Name'
FROM moodle.mdl_log l
	JOIN moodle.mdl_course c ON l.course = c.id
        JOIN moodle.mdl_user u   ON u.id = l.userid
	LEFT OUTER JOIN moodle.mdl_assignment a ON l.info = a.id,
	(SELECT @rank:=0) as t
WHERE l.time >= UNIX_TIMESTAMP(STR_TO_DATE(?, '%Y-%m-%d %T')) 
  AND l.time <= UNIX_TIMESTAMP(STR_TO_DATE(?, '%Y-%m-%d %T'))
  AND l.course=?
GROUP BY l.ip, l.userid) tt
GROUP BY tt.userid 
HAVING count(*) > 1;", array("$day $start", "$day $end","$courseid"));

echo "<h1>Bad Accounts</h1>Accounts used by more than one IP address.<br/>";
//my_print_table($result);

$count = 0;
$bad_accounts=array_keys($result);
foreach($bad_accounts as $acc){
	$result = $DB->get_records_sql("
	SELECT @rank:=@rank+1 as 'rank', l.ip, CONCAT(u.firstname, ' ', u.lastname) as 'Name'
	FROM moodle.mdl_log l
		JOIN moodle.mdl_course c ON l.course = c.id
	        JOIN moodle.mdl_user u   ON u.id = l.userid
		LEFT OUTER JOIN moodle.mdl_assignment a ON l.info = a.id,
		(SELECT @rank:=0) as t
	WHERE l.time >= UNIX_TIMESTAMP(STR_TO_DATE(?, '%Y-%m-%d %T')) 
	  AND l.time <= UNIX_TIMESTAMP(STR_TO_DATE(?, '%Y-%m-%d %T'))
	  AND l.course=?
	  AND l.userid =? 
	GROUP BY l.ip, l.userid;", array("$day $start", "$day $end","$courseid", $acc));
	if($count == 0){
		echo "<table border='1'>";
		echo "<tr>";
		foreach($result as $row){
			foreach($row as $col => $val){
				echo "<td>" . $col . "</td>";
				$count = $count + 1;
			}
			break;
		}
		echo "</tr>";
	}

	foreach($result as $id => $row){
		echo "<tr>";
		foreach($row as $col => $val){
			echo "<td>" . $val . "</td>";
		}
		echo "<tr/>";
	}
	echo "<tr style='border-bottom: 5px solid; background-color: #EEEEEE;'><td colspan='$count'>&nbsp;</td></tr>";
}
echo "</table>";
}



?>
</body>
</html>
