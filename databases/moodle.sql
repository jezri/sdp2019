-- MySQL dump 10.13  Distrib 5.7.25, for Linux (x86_64)
--
-- Host: localhost    Database: Moodle
-- ------------------------------------------------------
-- Server version	5.7.25-0ubuntu0.18.10.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `DATA`
--

DROP TABLE IF EXISTS `DATA`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DATA` (
  `ID` int(11) NOT NULL,
  `TEXT` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `DATA`
--

LOCK TABLES `DATA` WRITE;
/*!40000 ALTER TABLE `DATA` DISABLE KEYS */;
INSERT INTO `DATA` VALUES (1,'First row entry.'),(2,'Second row entry.'),(3,'Third row entry.'),(4,'Fourth row entry'),(5,'Fifth row entry.');
/*!40000 ALTER TABLE `DATA` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mdl_course`
--

DROP TABLE IF EXISTS `mdl_course`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mdl_course` (
  `id` int(11) NOT NULL,
  `fullname` varchar(250) DEFAULT NULL,
  `shortname` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mdl_course`
--

LOCK TABLES `mdl_course` WRITE;
/*!40000 ALTER TABLE `mdl_course` DISABLE KEYS */;
INSERT INTO `mdl_course` VALUES (1,'Course A','CA'),(2,'Course B','CB'),(3,'Course C','CC'),(4,'Course D','CD'),(5,'Course E','CE');
/*!40000 ALTER TABLE `mdl_course` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mdl_logstore_standard_log`
--

DROP TABLE IF EXISTS `mdl_logstore_standard_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mdl_logstore_standard_log` (
  `id` int(11) NOT NULL,
  `userid` int(11) DEFAULT NULL,
  `courseid` int(11) DEFAULT NULL,
  `timecreated` bigint(10) DEFAULT NULL,
  `ip` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mdl_logstore_standard_log`
--

LOCK TABLES `mdl_logstore_standard_log` WRITE;
/*!40000 ALTER TABLE `mdl_logstore_standard_log` DISABLE KEYS */;
INSERT INTO `mdl_logstore_standard_log` VALUES (1,5,1,1554354851,'197.91.172.109'),(2,6,4,1554355071,'116.211.85.110'),(3,5,1,1554358326,'41.247.187.14'),(4,5,1,1554358620,'197.91.172.109'),(5,6,1,1554358621,'116.211.85.110'),(6,2,1,1554360430,'10.100.5.49'),(7,2,1,1554360439,'197.91.172.109'),(8,2,1,1554362231,'41.247.187.14'),(9,2,1,1554362280,'41.247.187.14'),(10,3,1,1554362292,'41.247.187.14'),(11,5,1,1554362377,'197.91.172.109'),(12,2,1,1554362441,'197.91.172.109'),(13,1,1,1554362464,'197.91.172.109'),(14,2,1,1554365702,'41.247.187.14'),(15,4,2,1554451206,'10.100.5.47'),(16,4,2,1554451293,'10.100.5.48'),(17,6,3,1554462951,'187.159.197.67'),(18,6,3,1554462952,'5.80.171.42'),(19,1,2,1554462953,'10.100.5.49'),(20,4,2,1554462972,'10.100.5.49'),(21,4,5,1554462981,'187.159.197.67'),(22,3,5,1554463071,'10.100.5.49'),(23,3,3,1554466032,'10.198.20.15'),(24,3,3,1554466454,'10.198.20.83'),(25,3,3,1554466888,'146.141.14.60'),(26,6,2,1554467400,'5.80.171.42'),(27,7,2,1554470151,'5.80.171.42'),(28,3,3,1554470177,'10.100.5.47'),(29,2,3,1554470481,'10.100.5.47'),(30,4,3,1554471910,'10.100.5.47');
/*!40000 ALTER TABLE `mdl_logstore_standard_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mdl_user`
--

DROP TABLE IF EXISTS `mdl_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mdl_user` (
  `id` int(11) NOT NULL,
  `username` varchar(50) DEFAULT NULL,
  `firstname` varchar(50) DEFAULT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mdl_user`
--

LOCK TABLES `mdl_user` WRITE;
/*!40000 ALTER TABLE `mdl_user` DISABLE KEYS */;
INSERT INTO `mdl_user` VALUES (1,'firstuser','First','User'),(2,'seconduser','Second','User'),(3,'thirduser','Third','User'),(4,'fourthuser','Fourth','User'),(5,'fifthuser','Fifth','User'),(6,'sixthuser','Sixth','User'),(7,'seventhuser','Seventh','User');
/*!40000 ALTER TABLE `mdl_user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-04-08  7:09:23
