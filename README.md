# sdp2019

The Software Design project 2019 focuses on enforcing stricter marking for moodle tests. This is achieved by checking if multiple IP addresses are associated with a single user account or if a single user account is associated with multiple IP addresses.
Furthermore the project aims to identify plagarism in any code submitted.

# Pipeline status

[![pipeline status](https://gitlab.com/jezri/sdp2019/badges/master/pipeline.svg)](https://gitlab.com/jezri/sdp2019/commits/master)

# Coverage report

## Coverage report on master

[![coverage report](https://gitlab.com/jezri/sdp2019/badges/master/coverage.svg)](https://gitlab.com/jezri/sdp2019/commits/master)


## Coverage report on dev

[![coverage report](https://gitlab.com/jezri/sdp2019/badges/dev/coverage.svg)](https://gitlab.com/jezri/sdp2019/commits/master)


