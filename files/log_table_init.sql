use Moodle

INSERT INTO mdl_logstore_standard_log VALUES

/*
	Below are two periods separated into two days (4th and 5th of April 2019.
	The 4th of April consists of events from one sub-period: 08:00 to 10:00.
	The 5th of April consists of events from two sub-periods: 10:00 to 12:00 and 14:00 to 16:00.

	Each period consists of two instances of each case i.e each period consists of:
		- two instances of one account associated with multiple ip addresses
		- two instances of one ip address associated with multiple accounts
*/

	(1, 5, 1, UNIX_TIMESTAMP('2019-04-04 07:14:11'), '197.91.172.109'),		-- Regular data (flagged ip)
	(2, 6, 4, UNIX_TIMESTAMP('2019-04-04 07:17:51'), '116.211.85.110'),		-- Regular data

-- Beginning of the first period
	
	-- One Account, multiple ip addresses
	(3, 5, 1, UNIX_TIMESTAMP('2019-04-04 08:12:06'), '41.247.187.14'),		-- First instance
	(4, 5, 1, UNIX_TIMESTAMP('2019-04-04 08:17:00'), '197.91.172.109'),

	(5, 6, 1, UNIX_TIMESTAMP('2019-04-04 08:17:01'), '116.211.85.110'),		-- Regular data

	(6, 2, 1, UNIX_TIMESTAMP('2019-04-04 08:47:10'), '10.100.5.49'),			-- Second instance
	(7, 2, 1, UNIX_TIMESTAMP('2019-04-04 08:47:19'), '197.91.172.109'),
	(8, 2, 1, UNIX_TIMESTAMP('2019-04-04 09:17:11'), '41.247.187.14'),

	-- One ip address, multiple accounts
	(9, 2, 1, UNIX_TIMESTAMP('2019-04-04 09:18:00'), '41.247.187.14'),		-- First instance
	(10, 3, 1, UNIX_TIMESTAMP('2019-04-04 09:18:12'), '41.247.187.14'),

	(11, 5, 1, UNIX_TIMESTAMP('2019-04-04 09:19:37'), '197.91.172.109'),		-- Second instance
	(12, 2, 1, UNIX_TIMESTAMP('2019-04-04 09:20:41'), '197.91.172.109'),
	(13, 1, 1, UNIX_TIMESTAMP('2019-04-04 09:21:04'), '197.91.172.109'),

	(14, 2, 1, UNIX_TIMESTAMP('2019-04-04 10:15:02'), '41.247.187.14'),		-- Regular data (flagged ip)


-- Beginning of the second period
  -- Beginning of the first sub-period

	-- One Account, multiple ip addresses
	(15, 4, 2, UNIX_TIMESTAMP('2019-04-05 10:00:06'), '10.100.5.47'),		-- First instance
	(16, 4, 2, UNIX_TIMESTAMP('2019-04-05 10:01:33'), '10.100.5.48'),

	(17, 6, 3, UNIX_TIMESTAMP('2019-04-05 13:15:51'), '187.159.197.67'),	-- Regular data
	(18, 6, 3, UNIX_TIMESTAMP('2019-04-05 13:15:52'), '5.80.171.42'),		-- Regular data

	-- One ip address, multiple accounts
	(19, 1, 2, UNIX_TIMESTAMP('2019-04-05 13:15:53'), '10.100.5.49'),		-- First instance
	(20, 4, 2, UNIX_TIMESTAMP('2019-04-05 13:16:12'), '10.100.5.49'),

	
	(21, 4, 5, UNIX_TIMESTAMP('2019-04-05 13:16:21'), '187.159.197.67'),	-- Regular data (flagged account)
	(22, 3, 5, UNIX_TIMESTAMP('2019-04-05 13:17:51'), '10.100.5.49'),		-- Regular data (flagged ip)

  -- Beginning of the second sub-period

	-- One Account, multiple ip addresses
	(23, 3, 3, UNIX_TIMESTAMP('2019-04-05 14:07:12'), '10.198.20.15'),	-- Second instance
	(24, 3, 3, UNIX_TIMESTAMP('2019-04-05 14:14:14'), '10.198.20.83'),
	(25, 3, 3, UNIX_TIMESTAMP('2019-04-05 14:21:28'), '146.141.14.60'),

	(26, 6, 2, UNIX_TIMESTAMP('2019-04-05 14:30:00'), '5.80.171.42'),		-- Regular data
	(27, 7, 2, UNIX_TIMESTAMP('2019-04-05 15:15:51'), '5.80.171.42'),		-- Regular data

	-- One ip address, multiple accounts
	(28, 3, 3, UNIX_TIMESTAMP('2019-04-05 15:16:17'), '10.100.5.47'),		-- Second instance
	(29, 2, 3, UNIX_TIMESTAMP('2019-04-05 15:21:21'), '10.100.5.47'),
	(30, 4, 3, UNIX_TIMESTAMP('2019-04-05 15:45:10'), '10.100.5.47')
;
