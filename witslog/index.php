<?php
	require(__DIR__.'/../../config.php');
	require_once($CFG->libdir.'/adminlib.php');
	require "$CFG->libdir/tablelib.php";

	admin_externalpage_setup('reportwitslog', '', null, '', array('pagelayout'=>'report'));

	$showlogs      = optional_param('showlogs', false, PARAM_BOOL);
	$date          = optional_param('date', 0, PARAM_INT);
	$starttime     = optional_param('starttime', 0, PARAM_INT);
	$endtime       = optional_param('endtime', 0, PARAM_INT);
	$courseid      = optional_param('courseid', 0, PARAM_INT);
	$showaccounts  = optional_param('showaccounts', false, PARAM_BOOL);
	$showips       = optional_param('showips', false, PARAM_BOOL);


	$params = array();
	if ($showlogs) {
		$params['showlogs'] = $showlogs;
	}

	if ($date !== 0) {
		$params['date'] = $date;
	}

	if ($starttime !== 0) {
		$params['starttime'] = $starttime;
	}

	if ($endtime !== 0) {
		$params['endtime'] = $endtime;
	}

	if ($courseid !== 0) {
		$params['courseid'] = $courseid;
	}

	if ($showaccounts) {
		$params['showaccounts'] = $showaccounts;
	}

	if ($showips) {
		$params['showips'] = $showips;
	}

	$url = new moodle_url("/report/witslog/index.php", $params);

	//Note: remember to set page heading somewhere here
	$PAGE->set_url('/report/witslog/index.php');
	$PAGE->set_title(get_string('logs').': '.get_string('pluginname', 'report_witslog'));
	$PAGE->set_heading($SITE->shortname);
	
	$witslog = new report_witslog_renderable(true, $showlogs, $url, $date, $starttime, $endtime, $courseid, $showaccounts, $showips);

	$output = $PAGE->get_renderer('report_witslog');

	echo $output->header();
	echo $output->heading(get_string('pluginname', 'report_witslog')); //change this to something more approprieate
	
	if ($showlogs) {
		$witslog->setup_table();
	}

	echo $output->render($witslog);

	echo $output->footer();


