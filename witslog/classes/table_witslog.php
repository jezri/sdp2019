<?php

class report_witslog_table_witslog extends table_sql {
    //userfullnames -> possibly to reduce the number of database queries by keeping a list of usernames in memory
    private $userfullnames = array();

    //array | list of course full names
    private $course_fullnames = array();

    //stdClass | attributes are used to filter data displayed in the table
    private $filterparams;

    public function __construct($uniqueid, $filterparams = null) {
        parent::__construct($uniqueid);

        $this->filterparams = $filterparams;

        if ($filterparams->showaccounts) {
            $this->define_columns(array('user'));
            $this->define_headers(array(
                get_string('fullnameuser'),
            ));
        } elseif($filterparams->showips) {
            $this->define_columns(array('ip'));
            $this->define_headers(array(
                get_string('ip_address'),
            ));
        }
    }

    //format Time Column
    //Note: may no longer need this
    public function col_timecreated($record) {
        $dateformat = get_string('strftimedatetime', 'core_langconfig');
        return userdate($record->timecreated, $dateformat);
    }


    //format user column
    //Note: look into 'realuserid' and 'logextra'
    public function col_user($record) {
        if ($record->userid != 0) {
            $params = array('id' => $record->userid);
            //Note: remember to send course id ot user/view.php when it [userid] becomes available
            $username = $this->get_user_fullname($record->userid); //$record->$userid);
            $username = html_writer::link(new moodle_url('/user/view.php', $params), $username)
            . "&nbsp&nbsp&nbsp" . html_writer::link("#target" . $record->userid, html_writer::start_span('fa fa-caret-down') . html_writer::end_span(), array('data-toggle' => 'collapse'));

            $log_string = $this->get_log_data_string($record->userid);

            $username .= html_writer::empty_tag('br')
            . html_writer::start_tag('p', array('class' => 'collapse', 'data-toggle' => 'collapse', 'id' => 'target' . $record -> userid))
            . $log_string
            . html_writer::end_tag('p');

            return $username;
        }
        
        return NUll;

    }

    public function col_ip($record) {
        if ($record->userid != 0) {
            $ip = $record->ip;
            $ip .= "&nbsp&nbsp&nbsp" 
            . html_writer::link("#target" . $record->userid, html_writer::start_span('fa fa-caret-down') 
            . html_writer::end_span(), array('data-toggle' => 'collapse'));
            
            $log_string = $this->get_log_data_string($record->userid);
            
            $ip .= html_writer::empty_tag('br')
            . html_writer::start_tag('p', array('class' => 'collapse', 'data-toggle' => 'collapse', 'id' => 'target' . $record -> userid))
            . $log_string
            . html_writer::end_tag('p');

            return $ip;
        }

        return Null;
    }

    //format course column
    //Note: may no longer need this
    public function col_course($record) {
        $course_fullname = $this->get_course_fullname($record->courseid);
        return $course_fullname;
    }

    //overloading parent method table_sql::query_db
    public function query_db($pagesize, $useinitialsbar = true) {  //NOTE: handle page size and pagination
        global $DB;
        
        $inner_query = "SELECT id,timecreated,courseid,userid,ip,action FROM {logstore_standard_log}";
        $conditions = array();

        if (!empty($this->filterparams->courseid) && $this->filterparams->courseid != SITEID) {
            array_push($conditions, "courseid = " . $this->filterparams->courseid);
        }

        if (!empty($this->filterparams->date) && $this->filterparams->date != 0) {                                              //if date is set get records between:
            $format_string = "timecreated BETWEEN %d AND %d";

            if (!empty($this->filterparams->starttime) && $this->filterparams->starttime != 0) { 

                if (!empty($this->filterparams->endtime) && $this->filterparams->endtime != 0) {                                //starttime and endtime if both are set
                    $condition = sprintf($format_string, $this->filterparams->starttime, $this->filterparams->endtime);
                    array_push($conditions, $condition);
                } else {                                                                                                        //starttime and the end of the current day if only start time is set
                    $condition = sprintf($format_string, $this->filterparams->starttime, $this->filterparams->date + DAYSECS);
                    array_push($conditions, $condition);
                }

            } else {                                                                                                            //the whole day if only date is set
                $condition = sprintf($format_string, $this->filterparams->date, $this->filterparams->date + DAYSECS);           //DAYSECS = 24hrs in seconds. Each date is set at time 00:00:00am.
                array_push($conditions, $condition);
            }
        }

        if (sizeof($conditions) > 0) {
            $conditions = array_reverse($conditions);

            $inner_query .= " WHERE " . array_pop($conditions);

            foreach($conditions as $condition) {
                $inner_query .= " AND $condition";
            }
        }

        $query = "";
        if ($this->filterparams->showaccounts) {
            $query = "SELECT t.userid FROM (" . $inner_query . ") t GROUP BY t.userid HAVING COUNT(*) > 1";
        } else {
            $query = "SELECT t.ip FROM (" . $inner_query . ") t GROUP BY t.ip HAVING COUNT(*) > 1";
        }

        $this->rawdata = $DB->get_records_sql($query);
    }


    //helper function used to get the fullnames of users
    public function get_user_fullname($userid) {
        global $DB;

        if (!empty($this->userfullnames[$userid])) {
            return $this->userfullnames[$userid];
        }

        list($in_or_equal, $query_params) = $DB->get_in_or_equal($userid);
        $sql = "SELECT id," . get_all_user_name_fields(true) . " FROM {user} WHERE id " . $in_or_equal; //id included because first the first column of a select query must be a unique value
        
        $user = $DB->get_record_sql($sql, $query_params);
        
        $this->userfullnames[$userid] = fullname($user);
        return $this->userfullnames[$userid];
    }

    //helper function used to get the fullnames of courses
    public function get_course_fullname($courseid) {
        global $DB;

        if (!empty($this->course_fullnames[$courseid])) {
            return $this->course_fullnames[$courseid];
        }

        list($in_or_equal, $query_params) = $DB->get_in_or_equal($courseid);
        $sql = "SELECT * FROM {course} WHERE id" . $in_or_equal;

        $course = $DB->get_record_sql($sql, $query_params);

        if ($courseid == SITEID) {
            $this->$course_fullnames[$courseid] = get_course_display_name_for_list($course) .  ' (' . get_string('site') . ')';
        } else {
            $this->$course_fullnames[$courseid] = get_course_display_name_for_list($course);
        }

        return $this->$course_fullnames[$courseid];

    }

// TESTBLOCK START

    /**
     * Generate html code for the passed row.
     *
     * @param array $row Row data.
     * @param string $classname classes to add.
     *
     * @return string $html html code for the row passed.
     */
    public function get_row_html($row, $classname = '') {
        static $suppress_lastrow = NULL;
        $rowclasses = array();

        if ($classname) {
            $rowclasses[] = $classname;
        }

        $rowid = $this->uniqueid . '_r' . $this->currentrow;
        $html = '';

        $html .= html_writer::start_tag('tr', array('class' => implode(' ', $rowclasses), 'id' => $rowid));

        // If we have a separator, print it
        if ($row === NULL) {
            $colcount = count($this->columns);
            $html .= html_writer::tag('td', html_writer::tag('div', '',
                    array('class' => 'tabledivider')), array('colspan' => $colcount));

        } else {
            $colbyindex = array_flip($this->columns);
            foreach ($row as $index => $data) {
                $column = $colbyindex[$index];

                if (empty($this->prefs['collapse'][$column])) {
                    if ($this->column_suppress[$column] && $suppress_lastrow !== NULL && $suppress_lastrow[$index] === $data) {
                        $content = '&nbsp;';
                    } else {
                        $content = $data;
                    }
                } else {
                    $content = '&nbsp;';
                }

                $html .= html_writer::tag('td', $content, array(
                        'class' => 'cell c' . $index . $this->column_class[$column],
                        'id' => $rowid . '_c' . $index,
                        'style' => $this->make_styles_string($this->column_style[$column])));
            }
        }

        $html .= html_writer::end_tag('tr');

        $suppress_enabled = array_sum($this->column_suppress);
        if ($suppress_enabled) {
            $suppress_lastrow = $row;
        }
        $this->currentrow++;
        return $html;
    }

// echo "<br/><br/><br/><br/>Hello, World !!<br/><br/><br/><br/>";

    public function get_log_data_string($userid) {
        global $DB;

        $query = "SELECT action FROM {logstore_standard_log} WHERE userid = " . $userid;
        $records = $DB->get_records_sql($query);
        $log_string = "";
        foreach ($records as $record) {
            $log_string .= html_writer::empty_tag('br')
            . $record -> action;
        }
        return $log_string;
    }
// TESTBLOCK END
}