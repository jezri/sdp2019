<?php

defined('MOODLE_INTERNAL') || die;

class report_witslog_renderer extends plugin_renderer_base {
    protected function render_report_witslog(report_witslog_renderable $witslog) {
        //render selector form if 'show_form' is true
        if ($witslog->show_form) {
            $this->report_selector_form($witslog);
        }

        //render table if 'show_logs' is true
        if ($witslog->show_logs) {
            $witslog->table->out(10, true);
        }

    }

    public function report_selector_form(report_witslog_renderable $witslog) {
        echo html_writer::start_tag('form', array('class' => 'witslogbutton', 'action' => $witslog->url, 'method' => 'get'));  //<form ...>
        echo html_writer::start_div();

        //if this function if running, then 'showlogs' is true; this ensures that this is sent as a request parameter
        echo html_writer::empty_tag('input', array('type' => hidden, 'name' => 'showlogs', 'value' => '1'));


        //Add Course selector
        $courses = $witslog->get_course_list();
        echo html_writer::select($courses, 'courseid', $witslog->courseid, get_string('allcourses', 'report_witslog')); //<select ...>

        echo html_writer::empty_tag('br');                                                                          //<br/>
        echo html_writer::empty_tag('br');                                                                          //<br/>

        //Add date selector
        $dates = $witslog->get_date_options();
        echo html_writer::select($dates, 'date', $witslog->date, get_string('alldays', 'report_witslog'));          //<select ...>

        echo html_writer::empty_tag('br');                                                                          //<br/>
        echo html_writer::empty_tag('br');                                                                          //<br/>

        //Add time selector
        //display start-time selector only when a day has been selected
        if ($witslog->date != 0) {
            // echo "Note: check that starttime > endtime" . html_writer::empty_tag('br'); //note
            $times = $witslog->get_time_options($witslog->date);
            echo html_writer::select($times, 'starttime', $witslog->starttime, get_string('starttime', 'report_witslog'), array('class' => 'mb-1')); //<select ...>
        }

        echo html_writer::empty_tag('br');                                                                          //<br/>

        //display end-time selector only when a start-time has been selected
        if ($witslog->starttime != 0) {
            $times = $witslog->get_time_options($witslog->starttime, true);
            echo html_writer::select($times, 'endtime', $witslog->endtime, get_string('endtime', 'report_witslog'), array('class' => 'mt-1')); //<select ...>
        }
        

        echo html_writer::empty_tag('br');                                                                          //<br/>
        echo html_writer::empty_tag('br');                                                                          //<br/>


        echo html_writer::start_tag('button', array('type' => 'submit', 'name' => 'showaccounts', 'value' => '1',
                'class' => 'btn btn-secondary mr-2'))                                                               //<button ...>

                . get_string('getbadaccounts', 'report_witslog')

        . html_writer::end_tag('button');                                                                           //</button>


        echo html_writer::start_tag('button', array('type' => 'submit', 'name' => 'showips', 'value' => '1',
                'class' => 'btn btn-secondary mr-2'))                                                               //<button ...>

                . get_string('getbadips', 'report_witslog')

        . html_writer::end_tag('button');                                                                           //</button>

        echo html_writer::end_div();                                                                                 //</div>
        echo html_writer::end_tag('form');                                                                           //</form>
    }
}

